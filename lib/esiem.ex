defmodule ESIEM do
  @moduledoc """
  Documentation for `ESIEM`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ESIEM.hello()
      :world

  """
  def hello do
    :world
  end
end
