# e-SIEM
SIEM built using Elixir

<!-- Generate Elixir Project  `mix new e-siem --module ESIEM --app esiem` -->
<!-- $PATH=directory, $MODULE=MODULE,$APP=app-->

## :pushpin: Motivation 
- Design is loosely based on this SANS white-paper: [Creating Your Own SIEM and Incident Response Toolkit Using Open Source Tools](https://www.sans.org/white-papers/33689/)
<!-- Direct PDF: https://sansorg.egnyte.com/dl/DQirMFM1SI -->

## :memo: Description

## :hammer: How to Build

### Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `esiem` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:esiem, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/esiem>.

## :alembic: Usage 

## :triangular_ruler: Architecture

## :bricks:  Infrastructure

## :microscope: Technologies

## :card_file_box: Directory Explanation

## :blue_book: Technical Details

## :books: Resources
